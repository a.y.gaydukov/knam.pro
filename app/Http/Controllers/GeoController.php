<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeoController extends Controller
{
    public function cities()
    {
        $cities = DB::select(DB::raw("SELECT 
		  		t.id, 
		  		COALESCE(t.name_ru,t.name_en) AS name,
		  		r.name_ru AS r_name,
		  		r.id AS r_id,
		  		c.name_ru AS c_name,
		  		c.id AS c_id
		  		FROM sxgeo_cities t
		  		LEFT JOIN sxgeo_regions r
		  		ON r.id = t.region_id
		  		LEFT JOIN sxgeo_countries c
		  		ON c.iso = r.country 
		  		WHERE c.id IN (36,122,222,185)
                ORDER BY FIELD(t.id,508034,498817,495518,524901) DESC"));
        $result = [];
        foreach ($cities as $key => $value) {
            if (!isset($result[$value->c_id])) {
                $result[$value->c_id] = [
                    'id' => 'c'.$value->c_id,
                    'name' => $value->c_name,
                    'children' => []
                ];
            }
            if (!isset($result[$value->c_id]['children'][$value->r_id])) {
                $result[$value->c_id]['children'][$value->r_id] = [
                    'id' => 'r'.$value->r_id,
                    'name' => $value->r_name,
                    'children' => []
                ];
            }
            $result[$value->c_id]['children'][$value->r_id]['children'][] = [
                'id' => 't'.$value->id,
                'name' => $value->name
            ];
        }

        $cities = [];
        foreach ($result as $key => $value) {
            $cities[] = $this->getArrayWithoutKey($value);
        }
        return $cities;
    }

    public function sitecities(Request $request)
    {
        $id = (int)$request->input('id');
        $checkeds = [];
        if ($id) {
            $items = DB::select(DB::raw(
                "SELECT * FROM geo_filter_options WHERE site_id = ".$id
            ));
            foreach ($items as $key => $value) {
                switch ($value->el_type) {
                    case 1:
                        $checkeds[] = 'c'.$value->el_id;
                        break;

                    case 2:
                        $checkeds[] = 'r'.$value->el_id;
                        break;

                    case 3:
                        $checkeds[] = 't'.$value->el_id;
                        break;

                    default:
                        # code...
                        break;
                }
            }
        }
        return $checkeds;
    }

    private function getArrayWithoutKey($value)
    {
        $arr = [
            'id' => $value['id'],
            'name' => $value['name']
        ];
        if (isset($value['children'])) {
            $arr['children'] = [];
            foreach ($value['children'] as $key => $value1) {
                $arr['children'][] = $this->getArrayWithoutKey($value1);
            }
            return $arr;
        } else {
            $arr['leaf'] = true;
            return $arr;
        }

    }
}
