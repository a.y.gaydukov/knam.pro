<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function user(Request $request)
    {
        return auth()->user();
    }

    public function index(Request $request)
    {
        if (auth()->user()->id == 1) {
            return User::all();
        }
        return auth()->user();
    }

    public function show(User $user)
    {
        if (auth()->user()->id == 1) {
            return $user;
        }
        return auth()->user();
    }

    public function update(Request $request, User $user)
    {
        if ($user->id !== auth()->user()->id && auth()->user()->id !== 1) {
            return response()->json('Unauthorized',401);
        }
        $params = $request->all();

        $update = [];
        if (isset($params['providers']))
        {
            $data = $request->validate([
                'providers' => 'required|json',
            ]);
            $update['providers'] = $params['providers'];
        }
        if (isset($params['on']))
        {
            $data = $request->validate([
                'on' => 'required|boolean',
            ]);
            $update['on'] = $params['on'];
        }
        if (isset($params['name']))
        {
            $data = $request->validate([
                'name' => 'required|min:3|max:32|unique:users,name,'.$user->id.',id',
            ]);
            $update['name'] = $params['name'];
        }
        if (isset($params['email']))
        {
            $data = $request->validate([
                'email' => 'required|email|unique:users,email,'.$user->id.',id',
            ]);
            $update['email'] = $params['email'];
        }
        if (isset($params['password']))
        {
            $data = $request->validate([
                'password' => 'required|min:4|max:32',
            ]);
            $update['password'] = Hash::make($params['password']);
        }
        if (auth()->user()->id == 1 && isset($params['money']))
        {
            $data = $request->validate([
                'money' => 'required|integer',
            ]);
            $update['money'] = $params['money'];
        }
        $user->update($update);
        return response($user,200);
    }

}
