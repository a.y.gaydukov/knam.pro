<?php

namespace App\Http\Controllers;

use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SiteController extends Controller
{
    public function allsites(Request $request)
    {
        return Site::get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Site::where('user_id',auth()->user()->id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string',
            'domain' => 'required|string',
            'providers' => 'required|json',
            'on' => 'nullable|boolean',
        ]);
        //$data[domain] = $this->getDomainByUrl($data[domain]);
        $domain = $this->getDomainByUrl($request->domain);
        if (!$domain) {
            return response()->json('Введите корректный домен сайта!',422);
        }
        if ($this->checkUniqueDomain($domain)) {
            return response()->json('Такой домен уже существует!',422);
        }
        $site = Site::create([
            'user_id' => auth()->user()->id,
            'title' => $request->title,
            'domain' => $domain,
            'providers' => $request->providers,
            'on' => $request->on == true
        ]);
        return response($site,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        if ($site->user_id !== auth()->user()->id) {
            return response()->json('Unauthorized',401);
        }
        $params = $request->all();
        $update = [];
        if (isset($params['title']))
        {
            $data = $request->validate([
                'title' => 'required|string',
            ]);
            $update['title'] = $params['title'];
        }
        if (isset($params['domain']))
        {
            $data = $request->validate([
                'domain' => 'required|string',
            ]);
            $domain = $this->getDomainByUrl($params['domain']);
            if (!$domain) {
                return response()->json('Введите корректный домен сайта',422);
            }
            if ($this->checkUniqueDomain($domain,$site->id)) {
                return response()->json('Таког домен уже существует!',422);
            }
            $update['domain'] = $domain;
        }

        if (isset($params['providers']))
        {
            $data = $request->validate([
                'providers' => 'required|json',
            ]);
            $update['providers'] = $params['providers'];
        }

        if (isset($params['on']))
        {
            $data = $request->validate([
                'on' => 'required|boolean',
            ]);
            $update['on'] = $params['on'];
        }

        if (isset($params['api']))
        {
            $data = $request->validate([
                'api' => 'required|boolean',
            ]);
            $update['api'] = $params['api'];
        }

        if (isset($params['api']) && isset($params['api_url']))
        {
            $data = $request->validate([
                'api_url' => 'required|string',
            ]);
            $update['api_url'] = $params['api_url'];
        }

        if (isset($params['geo']))
        {
            $data = $request->validate([
                'geo' => 'required|boolean',
            ]);
            $update['geo'] = $params['geo'];
            DB::table('geo_filter_options')->delete();
            DB::table('geo_filter_cities')->delete();
            foreach ($params['cities'] as $key => $value) {
                $el_type = 0;
                $el_id = (int)preg_replace("/[^0-9]/", '', $value);
                switch (substr($value, 0, 1)) {
                    case 'c':
                        $el_type = 1;
                        $this->set_cities_from_country($site->id,$el_id);
                        break;

                    case 'r':
                        $el_type = 2;
                        $this->set_cities_from_region($site->id,$el_id);
                        break;

                    case 't':
                        $el_type = 3;
                        $this->set_city($site->id,$el_id);
                        break;

                    default:
                        # code...
                        break;
                }
                DB::table('geo_filter_options')->insert(
                    ['site_id' => $site->id, 'el_type' => $el_type, 'el_id' => $el_id]
                );
            }
        }

        $site->update($update);
        return response($site,200);
    }

    private function set_city($site_id,$city_id) {
        DB::insert("INSERT INTO geo_filter_cities (site_id,city_id) VALUES (".$site_id.",".$city_id.") ON DUPLICATE KEY UPDATE city_id = ".$city_id);
    }

    private function set_cities_from_region($site_id,$region_id) {
        $cities = DB::select(DB::raw(
            "SELECT id FROM sxgeo_cities WHERE region_id = ".$region_id
        ));

        foreach ($cities as $key => $value) {
            $this->set_city($site_id,$value->id);
        }
    }

    private function set_cities_from_country($site_id,$country_id) {
        $cities = DB::select(DB::raw(
            "SELECT t.id FROM sxgeo_cities t LEFT JOIN sxgeo_regions r ON r.id = t.region_id LEFT JOIN sxgeo_countries c ON c.iso = r.country WHERE c.id = ".$country_id
        ));

        foreach ($cities as $key => $value) {
            $this->set_city($site_id,$value->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        if ($site->user_id !== auth()->user()->id) {
            return response()->json('Unauthorized',401);
        }

        $site->delete();

        return response()->json('Site deleted',200);
    }

    private function getDomainByUrl($url) {
        $url = str_replace('https://','',$url);
        $url = str_replace('http://','',$url);
        //$url = str_replace('www.','',$url);
        $fullurl = 'https://'.$url;
        return parse_url($fullurl) && isset(parse_url($fullurl)['host']) && parse_url($fullurl)['host'] ? parse_url($fullurl)['host'] : false;
    }

    private function checkUniqueDomain($domain,$id = 0) {
        return Site::where('domain',$domain)->where('id','<>',$id)->count();
    }
}
