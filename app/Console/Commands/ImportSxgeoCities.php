<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportSxgeoCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sxgeocities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import dump sxgeo cities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_0.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_1.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_2.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_3.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_4.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_5.sql'));
        DB::unprepared(file_get_contents('database/sqls/sxgeo_cities_6.sql'));
    }
}
