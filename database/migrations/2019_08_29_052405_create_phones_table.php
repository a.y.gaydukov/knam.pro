<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('visit_id');
            $table->unsignedBigInteger('site_id');
            $table->string('phone');
            $table->text('url');
            $table->text('ref');
            $table->timestamp('visit_time');
            $table->timestamps();
            $table->softDeletes();

            $table->index('visit_id');
            $table->index('site_id');
            $table->index('phone');
            $table->index('visit_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phones', function (Blueprint $table) {
            $table->dropIndex('visit_time');
            $table->dropIndex('phone');
            $table->dropIndex('site_id');
            $table->dropIndex('visit_id');
        });
        Schema::dropIfExists('phones');
    }
}
