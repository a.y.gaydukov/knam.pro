<?php

namespace App\Http\Controllers;

use App\Phone;
use App\Exports\PhonesExport;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $start = $request->input('start').' 00:00:00';
        $end = $request->input('end').' 23:59:59';
        return Phone::where('phones.user_id',$user->id)->where('phones.visit_time','>=',$start)->whereNull('phones.deleted_at')->where('phones.visit_time','<=',$end)->join('sites', 'phones.site_id', '=', 'sites.id')->join('providers', 'phones.provider_id', '=', 'providers.id')->select('phones.id','phones.phone','phones.visit_time','phones.created_at','sites.title AS s_title','providers.title AS p_title')->orderBy('visit_time','desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        if ($phone->user_id !== auth()->user()->id) {
            return response()->json('Unauthorized',401);
        }

        $result = [
            'id' => $phone->id,
            'visit_id' => $phone->visit_id,
            'phone' => $phone->phone,
            'site' => $phone->site->title ?? 'Удален',
            'provider' => $phone->provider->title ?? 'Удален',
            'pages' => $phone->getPages(),
            'utm' => $phone->getUtms(),
        ];
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function edit(Phone $phone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phone $phone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Phone $phone)
    {
        //
    }

    public function stats(Request $request) {
        $user = auth()->user();
        $start = $request->input('start').' 00:00:00';
	$end = $request->input('end').' 23:59:59';

        //$visits = DB::table('tbl_visits')->where('tbl_visits.created_at','>=',$start)->where('tbl_visits.created_at','<=',$end)->count();
        $result = [
            'phones' => DB::table('phones')->where('phones.user_id',$user->id)->where('phones.visit_time','>=',$start)->whereNull('phones.deleted_at')->where('phones.visit_time','<=',$end)->count(),
            'visits' => DB::table('tbl_visits')->where('tbl_visits.user_id',$user->id)->where('tbl_visits.created_at','>=',$start)->where('tbl_visits.created_at','<=',$end)->count(),
            'providers' => DB::table('providers')->whereNull('deleted_at')->count()
        ];
        return $result;
    }

    public function statproviders(Request $request)
    {
        $user = auth()->user();
        $start = $request->input('start').' 00:00:00';
        $end = $request->input('end').' 23:59:59';

        return DB::select(
            DB::raw("
                SELECT
                providers.*,
                IF (tbl_run_providers_stats.sends IS NULL, 0, tbl_run_providers_stats.sends) sends,
                IF (phones_stats.phones IS NULL, 0, phones_stats.phones) phones,
                IF (phones_get_stats.phones_get IS NULL, 0, phones_get_stats.phones_get) phones_get
                FROM
                providers
                LEFT JOIN (
                    SELECT
                    provider_id,
                    COUNT(*) as sends
                    FROM
		    tbl_run_providers
		    FORCE INDEX (stat_index)
                    WHERE
                    tbl_run_providers.created_at >= '".$start."'
                    AND tbl_run_providers.created_at <= '".$end."'
                    AND tbl_run_providers.user_id = ".$user->id."
                    GROUP BY
                    provider_id
                ) tbl_run_providers_stats ON tbl_run_providers_stats.provider_id = providers.id
                LEFT JOIN (
                    SELECT
                    phones.provider_id,
                    COUNT(phones.id) as phones
                    FROM
                    phones
                    WHERE
                    phones.visit_time >= '".$start."'
                    AND phones.visit_time <= '".$end."'
                    AND phones.user_id = ".$user->id."
                    AND phones.deleted_at IS NULL
                    GROUP BY
                    phones.provider_id
                ) phones_stats ON phones_stats.provider_id = providers.id
                LEFT JOIN (
                    SELECT
                    provider_id,
                    count(phones.id) as phones_get
                    FROM
                    phones
                    WHERE
                    phones.created_at >= '".$start."'
                    AND phones.created_at <= '".$end."'
                    AND phones.user_id = ".$user->id."
                    AND phones.deleted_at IS NULL
                    GROUP BY
                    phones.provider_id
                ) phones_get_stats ON phones_get_stats.provider_id = providers.id
                WHERE
                providers.deleted_at IS NULL
            ")
        );
    }

    public function statsites(Request $request)
    {
        $user = auth()->user();
        $start = $request->input('start').' 00:00:00';
        $end = $request->input('end').' 23:59:59';

        return DB::select(DB::raw("
            SELECT
            sites.*,
            If(
                visits_stats.visits IS NULL,
                0,
                visits_stats.visits
            ) as visits,
            IF(
                phones_stats.phones IS NULL,
                0,
                phones_stats.phones
            ) as phones
            FROM
            sites
            LEFT JOIN (
                SELECT
                tbl_visits.site_id as site_id,
                count(*) as visits
                FROM
		tbl_visits
		FORCE INDEX (created_at_site_id)
                WHERE
                tbl_visits.created_at >= '".$start."'
                AND tbl_visits.created_at <= '".$end."'
                GROUP BY
                tbl_visits.site_id
            ) as visits_stats ON visits_stats.site_id = sites.id
            LEFT JOIN (
                SELECT
                phones.site_id as site_id,
                count(*) as phones
                FROM
                phones
                WHERE
                phones.visit_time >= '".$start."'
                AND phones.visit_time <= '".$end."'
                AND phones.deleted_at IS NULL
                GROUP BY
                phones.site_id
            ) as phones_stats ON phones_stats.site_id = sites.id
            WHERE
            sites.user_id = ".$user->id."
            AND sites.deleted_at IS NULL
        "));
    }

    public function xlsxphones(Request $request)
    {
        $user_id = $request->input('uid');
        $start = $request->input('start').' 00:00:00';
        $end = $request->input('end').' 23:59:59';
        if ($request->input('format') == 'csv')
        {
            return (new PhonesExport)->forDates($start,$end, $user_id)->download('phones.csv',\Maatwebsite\Excel\Excel::CSV);
        } else {
            return (new PhonesExport)->forDates($start,$end, $user_id)->download('phones.xlsx',\Maatwebsite\Excel\Excel::XLSX);
        }
    }

    public function test(Request $request) {
        $start = $request->input('start').' 00:00:00';
        $end = $request->input('end').' 23:59:59';
        return (new PhonesExport)->forDates($start,$end)->download('phones.xlsx');
        // return DB::select(DB::raw("SELECT providers.*,(SELECT count(*) FROM tbl_run_providers WHERE tbl_run_providers.provider_id = providers.id) AS visits FROM providers"));
        // //$result = DB::raw('SELECT count(*) as visits FROM tbl_run_providers');
        // $result = DB::select( DB::raw("SELECT count(*) as visits FROM tbl_run_providers"));
        // dd($result);
    }
}
