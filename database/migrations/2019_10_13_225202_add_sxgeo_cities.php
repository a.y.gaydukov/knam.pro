<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSxgeoCities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sxgeo_cities', function (Blueprint $table) {
            $table->unsignedMediumInteger('id');
            $table->unsignedMediumInteger('region_id');
            $table->string('name_ru', 128);
            $table->string('name_en', 128);
            $table->decimal('lat');
            $table->decimal('lon');
            $table->string('okato', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sxgeo_cities');
    }
}
