<?php

namespace App\Exports;

use App\Phone;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class PhonesExport implements FromQuery, Responsable, WithMapping, WithHeadings, WithCustomCsvSettings
{
    use Exportable;

    private $fileName = 'phones.xlsx';
    private $start;
    private $end;
    private $user_id;

    /**
    * Optional Writer Type
    */
    private $writerType = Excel::XLSX;

    public function forDates($start,$end, $user_id)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user_id = $user_id;
        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return Phone::select('id','site_id','provider_id','phone','url','ref','visit_time')->where('phones.user_id',$this->user_id)->where('phones.visit_time','>=',$this->start)->where('phones.visit_time','<=',$this->end);
    }

    public function map($phone): array
    {
        return [
            $phone->id,
            $phone->site ? $phone->site->title : '',
            $phone->phone,
            $phone->provider->title,
            $phone->url,
            $phone->ref,
            $phone->visit_time,
        ];
    }

    public function headings():array
    {
        return [
            'Id',
            'Сайт',
            'Номер телефона',
            'Поставщик',
            'Страница идентификации',
            'Источник',
            'Дата'
        ];
    }

    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'windows-1251'
        ];
    }
}
