<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','on','jscode'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $casts = [
        'on' => 'boolean'
    ];
}
