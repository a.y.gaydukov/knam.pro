<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Phone extends Model
{
    protected $fillable = ['visit_id'];

    protected $hidden = ['updated_at','deleted_at'];

    public function site() {
        return $this->belongsTo('App\Site');
    }

    public function provider() {
        return $this->belongsTo('App\Provider');
    }

    public function getPages()
    {
        $pages = DB::table('tbl_pages')->select('title','ref','url','created_at')->where('visit_id',$this->visit_id)->get();
        if (count($pages) == 0) {
            $pages = [];
            $visit = DB::table('tbl_visits')->where('id',$this->visit_id)->get();
            if (count($visit)) {
                $arr = [
                    'title' => '',
                    'ref' => $visit[0]->ref,
                    'url' => $visit[0]->site,
                    'created_at' => $visit[0]->created_at
                ];
                $page = json_decode(json_encode($arr));
                $pages[] = $page;
            }
        }
        return $pages;
    }

    public function getUtms() {
        $pages = $this->getPages();
        $__rutms = [];
        if (count($pages)) {
            $ref = $pages[0]->ref;
            if (strpos($ref, 'utm_') !== false) {
                $__rutms = $this->parseURLParams($ref);
            }
        }
        $__vutms = [];
        if (count($pages)) {
            $url = $pages[0]->url;
            if (strpos($url, 'utm_') !== false) {
                $__vutms = $this->parseURLParams($url) ;
            }
        }
        return array_merge($__rutms, $__vutms);
    }

    public function parseURLParams($url){
        $url = substr($url, strrpos($url, "?")+1);
        $exploded = array();
        parse_str($url, $exploded);
        return $exploded;
    }
}
