<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','on','domain','providers','seconds','user_id','geo','api','api_url'];

    protected $hidden = ['updated_at','deleted_at'];

    protected $casts = [
        'on' => 'boolean',
        'geo' => 'boolean',
        'api' => 'boolean'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
