<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    Route::middleware('admin')->group(function() {
        Route::get('/statproviders','PhoneController@statproviders');
        Route::resource('/providers','ProviderController')->except(['create','edit']);
        Route::get('/allsites','SiteController@allsites');
    });
    Route::resource('/users', 'UserController')->except(['create','edit']);

    Route::get('/cities','GeoController@cities');
    Route::get('/sitecities','GeoController@sitecities');

    Route::get('/stats','PhoneController@stats');

    Route::get('/user', 'UserController@user');


    Route::resource('/sites','SiteController')->except(['create','edit']);
    Route::get('/statsites','PhoneController@statsites');

    Route::resource('/phones','PhoneController')->except(['create','edit']);

    Route::post('/logout','AuthController@logout');
});

Route::post('/login','AuthController@login');
Route::post('/register','AuthController@register');
Route::post('/generatetoken','AuthController@generatetoken');
Route::post('/autologin','AuthController@autologin');

