<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSxgeoRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sxgeo_regions', function (Blueprint $table) {
            $table->unsignedMediumInteger('id');
            $table->string('iso', 7);
            $table->string('country', 2);
            $table->string('name_ru', 128);
            $table->string('name_en', 128);
            $table->string('timezone', 30);
            $table->string('okato', 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sxgeo_regions');
    }
}
