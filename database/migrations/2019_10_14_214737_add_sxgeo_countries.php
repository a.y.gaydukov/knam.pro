<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSxgeoCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sxgeo_countries', function (Blueprint $table) {
            $table->unsignedMediumInteger('id');
            $table->string('iso', 2);
            $table->string('continent', 2);
            $table->string('name_ru', 128);
            $table->string('name_en', 128);
            $table->decimal('lat');
            $table->decimal('lon');
            $table->string('timezone', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sxgeo_countries');
    }
}
