<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client();
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Неправильный запрос. Пожалуйста введите логин или пароль!', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Неверен логин или пароль!', $e->getCode());
            }
            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function logout()
    {
        auth()->user()->tokens->each(function($token,$key) {
            $token->delete();
        });

        return response()->json('Logout successfully',200);
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        return User::create([
            'name' => $request->email,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'providers' => '[]'
        ]);
    }

    public function generatetoken(Request $request)
    {
        $request->validate([
            'uid' => ['required','integer'],
        ]);
        $token = Str::random(40);
        User::where('id', $request->input('uid'))
            ->update(['autologin_token' => $token]);
        return response()->json($token,200);
    }

    public function autologin(Request $request)
    {
        $request->validate([
            'token' => ['required', 'string', 'min:8', 'max:255'],
        ]);
        $token = $request->input('token');
        $user = User::where('autologin_token',$token)->firstOrFail();
        $user->update(['autologin_token' => null]);
        $access_token = $user->createToken('mytoken')->accessToken;
        return response()->json($access_token,200);
    }
}
